import 'dart:async';
import 'dart:io';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:collection/collection.dart';
import 'package:fluffychat/config/app_config.dart';
import 'package:fluffychat/config/themes.dart';
import 'package:fluffychat/pages/chat_list/chat_list_view.dart';
import 'package:fluffychat/pages/settings_security/settings_security.dart';
import 'package:fluffychat/pangea/controllers/pangea_controller.dart';
import 'package:fluffychat/pangea/extensions/pangea_room_extension.dart';
import 'package:fluffychat/pangea/utils/add_to_space.dart';
import 'package:fluffychat/pangea/utils/bot_name.dart';
import 'package:fluffychat/pangea/utils/chat_list_handle_space_tap.dart';
import 'package:fluffychat/pangea/utils/error_handler.dart';
import 'package:fluffychat/utils/famedlysdk_store.dart';
import 'package:fluffychat/utils/localized_exception_extension.dart';
import 'package:fluffychat/utils/matrix_sdk_extensions/client_stories_extension.dart';
import 'package:fluffychat/utils/platform_infos.dart';
import 'package:fluffychat/utils/tor_stub.dart'
    if (dart.library.html) 'package:tor_detector_web/tor_detector_web.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:uni_links/uni_links.dart';
import 'package:vrouter/vrouter.dart';

import '../../../utils/account_bundles.dart';
import '../../pangea/utils/firebase_analytics.dart';
import '../../utils/matrix_sdk_extensions/matrix_file_extension.dart';
import '../../utils/matrix_sdk_extensions/matrix_locals.dart';
import '../../utils/url_launcher.dart';
import '../../utils/voip/callkeep_manager.dart';
import '../../widgets/fluffy_chat_app.dart';
import '../../widgets/matrix.dart';

enum SelectMode {
  normal,
  share,
  select,
}

enum PopupMenuAction {
  settings,
  invite,
  newGroup,
  newSpace,
  setStatus,
  archive,
}

enum ActiveFilter {
  allChats,
  groups,
  messages,
  spaces,
}

class ChatList extends StatefulWidget {
  static BuildContext? contextForVoip;

  const ChatList({Key? key}) : super(key: key);

  @override
  ChatListController createState() => ChatListController();
}

class ChatListController extends State<ChatList>
    with TickerProviderStateMixin, RouteAware {
  StreamSubscription? _intentDataStreamSubscription;

  StreamSubscription? _intentFileStreamSubscription;

  StreamSubscription? _intentUriStreamSubscription;

  bool get displayNavigationBar =>
      !FluffyThemes.isColumnMode(context) &&
      (spaces.isNotEmpty || AppConfig.separateChatTypes);

  String? activeSpaceId;

  void resetActiveSpaceId() {
    setState(() {
      activeSpaceId = null;
      //#Pangea
      VRouter.of(context).to("/rooms");
      //Pangea#
    });
  }

  void setActiveSpace(String? spaceId) {
    setState(() {
      activeSpaceId = spaceId;
      activeFilter = ActiveFilter.spaces;
    });
  }

  int get selectedIndex {
    switch (activeFilter) {
      case ActiveFilter.allChats:
      case ActiveFilter.messages:
        return 0;
      case ActiveFilter.groups:
        return 1;
      case ActiveFilter.spaces:
        return AppConfig.separateChatTypes ? 2 : 1;
    }
  }

  // #Pangea
  bool isSelected(int i) {
    if (activeFilter == ActiveFilter.spaces && activeSpaceId != null) {
      return false;
    }
    return i == selectedIndex;
  }
  // Pangea#

  ActiveFilter getActiveFilterByDestination(int? i) {
    switch (i) {
      case 1:
        if (AppConfig.separateChatTypes) {
          return ActiveFilter.groups;
        }
        return ActiveFilter.spaces;
      case 2:
        return ActiveFilter.spaces;
      case 0:
      default:
        if (AppConfig.separateChatTypes) {
          return ActiveFilter.messages;
        }
        return ActiveFilter.allChats;
    }
  }

  void onDestinationSelected(int? i) {
    setState(() {
      debugPrint('onDestinationSelected $i');

      activeFilter = getActiveFilterByDestination(i);
    });
    // #Pangea
    final bool clickedAllSpaces = (!AppConfig.separateChatTypes && i == 1) ||
        (AppConfig.separateChatTypes && i == 2);
    if (clickedAllSpaces) {
      setActiveSpace(null);
    }
    // Pangea#
  }

  ActiveFilter activeFilter = AppConfig.separateChatTypes
      ? ActiveFilter.messages
      : ActiveFilter.allChats;

  bool Function(Room) getRoomFilterByActiveFilter(ActiveFilter activeFilter) {
    switch (activeFilter) {
      case ActiveFilter.allChats:
        return (room) =>
            !room.isSpace &&
            !room.isStoryRoom
            // #Pangea
            &&
            !room.isAnalyticsRoom;
      // Pangea#;
      case ActiveFilter.groups:
        return (room) =>
            !room.isSpace &&
            !room.isDirectChat &&
            !room.isStoryRoom
            // #Pangea
            &&
            !room.isAnalyticsRoom;
      // Pangea#;;
      case ActiveFilter.messages:
        return (room) =>
            !room.isSpace &&
            room.isDirectChat &&
            !room.isStoryRoom
            // #Pangea
            &&
            !room.isAnalyticsRoom;
      // Pangea#;;
      case ActiveFilter.spaces:
        return (r) => r.isSpace;
    }
  }

  List<Room> get filteredRooms => Matrix.of(context)
      .client
      .rooms
      .where((Room room) => getRoomFilterByActiveFilter(activeFilter)(room)
          // #Pangea
          // &&
          // !room.isDirectChatWithoutMe
          )
      .sorted((roomA, roomB) => sortChatList(roomA, roomB))
      // Pangea#
      .toList();

  // #Pangea
  int sortChatList(Room roomA, Room roomB) {
    /* 
    returns an integer indicating the order of the two elements:
    negative -> the first element should be before the second element
    zero -> the first element at the same position
    positive -> the first element should be after

    This function applies a set of rules in order of sorting priority.
    The first priority is to put pangea bot at the top of the list
    This second priority is to put chats with the most 
    recent event at the top of the chat list
    */
    final bool aIsBot = roomA.directChatMatrixID == BotName.byEnvironment;
    final bool bIsBot = roomB.directChatMatrixID == BotName.byEnvironment;
    if (aIsBot && !bIsBot) return -1;
    if (!aIsBot && bIsBot) return 1;

    final DateTime? aLastTimestamp = roomA.lastEvent?.originServerTs;
    final DateTime? bLastTimestamp = roomB.lastEvent?.originServerTs;
    if (aLastTimestamp != null && bLastTimestamp != null) {
      return bLastTimestamp.compareTo(aLastTimestamp);
    }

    return 0;
  }

  int sortSpaceChildren(
      SpaceRoomsChunk childA, SpaceRoomsChunk childB, String activeSpaceId) {
    /* sorts rooms within space */
    final Room? roomA = Matrix.of(context).client.getRoomById(childA.roomId);
    final Room? roomB = Matrix.of(context).client.getRoomById(childB.roomId);

    if (childA.roomId == activeSpaceId) return -1;
    if (childB.roomId == activeSpaceId) return 1;

    // first, sort by bot rooms
    final bool aIsBot = roomA?.directChatMatrixID == BotName.byEnvironment;
    final bool bIsBot = roomB?.directChatMatrixID == BotName.byEnvironment;
    if (aIsBot && !bIsBot) return -1;
    if (!aIsBot && bIsBot) return 1;

    // next, sort by spaces to put exchanges at the top
    final bool aIsSpace = childA.roomType == 'm.space';
    final bool bIsSpace = childB.roomType == 'm.space';
    if (aIsSpace && !bIsSpace) return -1;
    if (!aIsSpace && bIsSpace) return 1;

    // next, sort by unjoined rooms
    // if a room is null, the most recent event cannot be determined
    // and the room has likely not been joined
    final bool aIsJoined = roomA != null;
    final bool bIsJoined = roomB != null;
    if (!aIsJoined && bIsJoined) return -1;
    if (aIsJoined && !bIsJoined) return 1;

    // finally, sort by most recent events in joined rooms
    final DateTime? aLastTimestamp = roomA?.lastEvent?.originServerTs;
    final DateTime? bLastTimestamp = roomB?.lastEvent?.originServerTs;
    if (aLastTimestamp != null && bLastTimestamp != null) {
      return bLastTimestamp.compareTo(aLastTimestamp);
    }

    return 0;
  }
  // Pangea#

  bool isSearchMode = false;
  Future<QueryPublicRoomsResponse>? publicRoomsResponse;
  String? searchServer;
  Timer? _coolDown;
  SearchUserDirectoryResponse? userSearchResult;
  QueryPublicRoomsResponse? roomSearchResult;

  bool isSearching = false;
  static const String _serverStoreNamespace = 'im.fluffychat.search.server';

  //#Pangea
  final PangeaController pangeaController = MatrixState.pangeaController;
  //Pangea#
  void setServer() async {
    final newServer = await showTextInputDialog(
      useRootNavigator: false,
      title: L10n.of(context)!.changeTheHomeserver,
      context: context,
      okLabel: L10n.of(context)!.ok,
      cancelLabel: L10n.of(context)!.cancel,
      textFields: [
        DialogTextField(
          prefixText: 'https://',
          hintText: Matrix.of(context).client.homeserver?.host,
          initialText: searchServer,
          keyboardType: TextInputType.url,
          autocorrect: false,
        )
      ],
    );
    if (newServer == null) return;
    Store().setItem(_serverStoreNamespace, newServer.single);
    setState(() {
      searchServer = newServer.single;
    });
    onSearchEnter(searchController.text);
  }

  final TextEditingController searchController = TextEditingController();

  void _search() async {
    final client = Matrix.of(context).client;
    if (!isSearching) {
      setState(() {
        isSearching = true;
      });
    }
    SearchUserDirectoryResponse? userSearchResult;
    QueryPublicRoomsResponse? roomSearchResult;
    try {
      roomSearchResult = await client.queryPublicRooms(
        server: searchServer,
        filter: PublicRoomQueryFilter(genericSearchTerm: searchController.text),
        limit: 20,
      );
      userSearchResult = await client.searchUserDirectory(
        searchController.text,
        limit: 20,
      );
    } catch (e, s) {
      Logs().w('Searching has crashed', e, s);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            e.toLocalizedString(context),
          ),
        ),
      );
    }
    if (!isSearchMode) return;
    setState(() {
      isSearching = false;
      this.roomSearchResult = roomSearchResult;
      this.userSearchResult = userSearchResult;
    });
  }

  void onSearchEnter(String text) {
    if (text.isEmpty) {
      cancelSearch(unfocus: false);
      return;
    }

    setState(() {
      isSearchMode = true;
    });
    _coolDown?.cancel();
    _coolDown = Timer(const Duration(milliseconds: 500), _search);
  }

  void cancelSearch({bool unfocus = true}) {
    setState(() {
      searchController.clear();
      isSearchMode = false;
      roomSearchResult = userSearchResult = null;
      isSearching = false;
    });
    if (unfocus) FocusManager.instance.primaryFocus?.unfocus();
  }

  bool isTorBrowser = false;

  BoxConstraints? snappingSheetContainerSize;

  final ScrollController scrollController = ScrollController();
  final ValueNotifier<bool> scrolledToTop = ValueNotifier(true);

  final StreamController<Client> _clientStream = StreamController.broadcast();

  Stream<Client> get clientStream => _clientStream.stream;

  void addAccountAction() => VRouter.of(context).to('/settings/account');

  void _onScroll() {
    final newScrolledToTop = scrollController.position.pixels <= 0;
    if (newScrolledToTop != scrolledToTop.value) {
      scrolledToTop.value = newScrolledToTop;
    }
  }

  void editSpace(BuildContext context, String spaceId) async {
    await Matrix.of(context).client.getRoomById(spaceId)!.postLoad();
    if (mounted) {
      VRouter.of(context).toSegments(['spaces', spaceId]);
    }
  }

  // Needs to match GroupsSpacesEntry for 'separate group' checking.
  List<Room> get spaces =>
      Matrix.of(context).client.rooms.where((r) => r.isSpace).toList();

  final selectedRoomIds = <String>{};

  String? get activeChat => VRouter.of(context).pathParameters['roomid'];

  SelectMode get selectMode => Matrix.of(context).shareContent != null
      ? SelectMode.share
      : selectedRoomIds.isEmpty
          ? SelectMode.normal
          : SelectMode.select;

  void _processIncomingSharedFiles(List<SharedMediaFile> files) {
    if (files.isEmpty) return;
    final file = File(files.first.path.replaceFirst('file://', ''));

    Matrix.of(context).shareContent = {
      'msgtype': 'chat.fluffy.shared_file',
      'file': MatrixFile(
        bytes: file.readAsBytesSync(),
        name: file.path,
      ).detectFileType,
    };
    VRouter.of(context).to('/rooms');
  }

  void _processIncomingSharedText(String? text) {
    if (text == null) return;
    if (text.toLowerCase().startsWith(AppConfig.deepLinkPrefix) ||
        text.toLowerCase().startsWith(AppConfig.inviteLinkPrefix) ||
        (text.toLowerCase().startsWith(AppConfig.schemePrefix) &&
            !RegExp(r'\s').hasMatch(text))) {
      return _processIncomingUris(text);
    }
    Matrix.of(context).shareContent = {
      'msgtype': 'm.text',
      'body': text,
    };
    VRouter.of(context).to('/rooms');
  }

  void _processIncomingUris(String? text) async {
    if (text == null) return;
    VRouter.of(context).to('/rooms');
    WidgetsBinding.instance.addPostFrameCallback((_) {
      UrlLauncher(context, text).openMatrixToUrl();
    });
  }

  void _initReceiveSharingIntent() {
    if (!PlatformInfos.isMobile) return;

    // For sharing images coming from outside the app while the app is in the memory
    _intentFileStreamSubscription = ReceiveSharingIntent.getMediaStream()
        .listen(_processIncomingSharedFiles, onError: print);

    // For sharing images coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialMedia().then(_processIncomingSharedFiles);

    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    _intentDataStreamSubscription = ReceiveSharingIntent.getTextStream()
        .listen(_processIncomingSharedText, onError: print);

    // For sharing or opening urls/text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then(_processIncomingSharedText);

    // For receiving shared Uris
    _intentUriStreamSubscription = linkStream.listen(_processIncomingUris);
    if (FluffyChatApp.gotInitialLink == false) {
      FluffyChatApp.gotInitialLink = true;
      getInitialLink().then(_processIncomingUris);
    }
  }

  //#Pangea
  StreamSubscription? classStream;
  StreamSubscription? _invitedSpaceSubscription;
  //Pangea#
  @override
  void initState() {
    _initReceiveSharingIntent();

    scrollController.addListener(_onScroll);
    _waitForFirstSync();
    _hackyWebRTCFixForWeb();
    CallKeepManager().initialize();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (mounted) {
        searchServer = await Store().getItem(_serverStoreNamespace);
        Matrix.of(context).backgroundPush?.setupPush();
      }
    });

    _checkTorBrowser();

    //#Pangea
    classStream = pangeaController.classController.stateStream.listen((event) {
      if (event["activeSpaceId"] != null && mounted) {
        setActiveSpace(event["activeSpaceId"]);
      }
    });

    _invitedSpaceSubscription = pangeaController
        .matrixState.client.onSync.stream
        .where((event) => event.rooms?.invite != null)
        .listen((event) {
      for (final inviteEntry in event.rooms!.invite!.entries) {
        if (inviteEntry.value.inviteState == null) continue;
        final bool isSpace = inviteEntry.value.inviteState!.any(
          (event) =>
              event.type == EventTypes.RoomCreate &&
              event.content['type'] == 'm.space',
        );
        if (!isSpace) continue;
        final String spaceId = inviteEntry.key;
        final Room? space = pangeaController.matrixState.client.getRoomById(
          spaceId,
        );
        if (space != null) {
          chatListHandleSpaceTap(
            context,
            this,
            space,
          );
        }
      }
    });
    //Pangea#

    super.initState();
  }

  @override
  void dispose() {
    _intentDataStreamSubscription?.cancel();
    _intentFileStreamSubscription?.cancel();
    _intentUriStreamSubscription?.cancel();
    //#Pangea
    classStream?.cancel();
    _invitedSpaceSubscription?.cancel();
    //Pangea#
    scrollController.removeListener(_onScroll);
    super.dispose();
  }

  void toggleSelection(String roomId) {
    setState(
      () => selectedRoomIds.contains(roomId)
          ? selectedRoomIds.remove(roomId)
          : selectedRoomIds.add(roomId),
    );
  }

  Future<void> toggleUnread() async {
    await showFutureLoadingDialog(
      context: context,
      future: () async {
        final markUnread = anySelectedRoomNotMarkedUnread;
        final client = Matrix.of(context).client;
        for (final roomId in selectedRoomIds) {
          final room = client.getRoomById(roomId)!;
          if (room.markedUnread == markUnread) continue;
          await client.getRoomById(roomId)!.markUnread(markUnread);
        }
      },
    );
    cancelAction();
  }

  Future<void> toggleFavouriteRoom() async {
    await showFutureLoadingDialog(
      context: context,
      future: () async {
        final makeFavorite = anySelectedRoomNotFavorite;
        final client = Matrix.of(context).client;
        for (final roomId in selectedRoomIds) {
          final room = client.getRoomById(roomId)!;
          if (room.isFavourite == makeFavorite) continue;
          await client.getRoomById(roomId)!.setFavourite(makeFavorite);
        }
      },
    );
    cancelAction();
  }

  Future<void> toggleMuted() async {
    await showFutureLoadingDialog(
      context: context,
      future: () async {
        final newState = anySelectedRoomNotMuted
            ? PushRuleState.mentionsOnly
            : PushRuleState.notify;
        final client = Matrix.of(context).client;
        for (final roomId in selectedRoomIds) {
          final room = client.getRoomById(roomId)!;
          if (room.pushRuleState == newState) continue;
          await client.getRoomById(roomId)!.setPushRuleState(newState);
        }
      },
    );
    cancelAction();
  }

  Future<void> archiveAction() async {
    final confirmed = await showOkCancelAlertDialog(
          useRootNavigator: false,
          context: context,
          title: L10n.of(context)!.areYouSure,
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        ) ==
        OkCancelResult.ok;
    if (!confirmed) return;
    // #Pangea
    final bool archivedActiveRoom =
        selectedRoomIds.contains(Matrix.of(context).activeRoomId);
    // Pangea#
    await showFutureLoadingDialog(
      context: context,
      future: () => _archiveSelectedRooms(),
    );
    setState(() {});
    // #Pangea
    if (archivedActiveRoom) {
      VRouter.of(context).to('/rooms');
    }
    // Pangea#
  }

  void setStatus() async {
    final input = await showTextInputDialog(
      useRootNavigator: false,
      context: context,
      title: L10n.of(context)!.setStatus,
      okLabel: L10n.of(context)!.ok,
      cancelLabel: L10n.of(context)!.cancel,
      textFields: [
        DialogTextField(
          hintText: L10n.of(context)!.statusExampleMessage,
        ),
      ],
    );
    if (input == null) return;
    await showFutureLoadingDialog(
      context: context,
      future: () => Matrix.of(context).client.setPresence(
            Matrix.of(context).client.userID!,
            PresenceType.online,
            statusMsg: input.single,
          ),
    );
  }

  Future<void> _archiveSelectedRooms() async {
    final client = Matrix.of(context).client;
    while (selectedRoomIds.isNotEmpty) {
      final roomId = selectedRoomIds.first;
      try {
        await client.getRoomById(roomId)!.leave();
      } finally {
        toggleSelection(roomId);
      }
    }
  }

  Future<void> addToSpace() async {
    final selectedSpace = await showConfirmationDialog<String>(
      context: context,
      title: L10n.of(context)!.addToSpace,
      message: L10n.of(context)!.addToSpaceDescription,
      fullyCapitalizedForMaterial: false,
      actions: Matrix.of(context)
          .client
          //#Pangea
          // .rooms
          // .where((r) => r.isSpace)
          .rooms
          .where((r) =>
              r.isSpace &&
              selectedRoomIds
                  .map((id) => Matrix.of(context).client.getRoomById(id))
                  .where((e) => !(e?.isPangeaClass ?? true))
                  .every((e) => r.canIAddSpaceChild(e)))
          //Pangea#
          .map(
            (space) => AlertDialogAction(
              key: space.id,
              label: space
                  .getLocalizedDisplayname(MatrixLocals(L10n.of(context)!)),
            ),
          )
          .toList(),
    );
    if (selectedSpace == null) return;
    final result = await showFutureLoadingDialog(
      context: context,
      future: () async {
        final space = Matrix.of(context).client.getRoomById(selectedSpace)!;
        // #Pangea
        await pangeaAddToSpace(
          space,
          selectedRoomIds.toList(),
          context,
          pangeaController,
        );
        // if (space.canSendDefaultStates) {
        //   for (final roomId in selectedRoomIds) {
        //     await space.setSpaceChild(roomId);
        //   }
        // }
        // Pangea#
      },
    );
    if (result.error == null) {
      if (!mounted) return;
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(L10n.of(context)!.chatHasBeenAddedToThisSpace),
        ),
      );
    }

    setState(() => selectedRoomIds.clear());
  }

  bool get anySelectedRoomNotMarkedUnread => selectedRoomIds.any(
        (roomId) =>
            !Matrix.of(context).client.getRoomById(roomId)!.markedUnread,
      );

  bool get anySelectedRoomNotFavorite => selectedRoomIds.any(
        (roomId) => !Matrix.of(context).client.getRoomById(roomId)!.isFavourite,
      );

  bool get anySelectedRoomNotMuted => selectedRoomIds.any(
        (roomId) =>
            Matrix.of(context).client.getRoomById(roomId)!.pushRuleState ==
            PushRuleState.notify,
      );

  bool waitForFirstSync = false;

  Future<void> _waitForFirstSync() async {
    final client = Matrix.of(context).client;
    await client.roomsLoading;
    await client.accountDataLoading;

    if (client.prevBatch == null) {
      await client.onSync.stream.first;

      // #Pangea
      pangeaController.startChatWithBotIfNotPresent();
      //Pangea#

      // Display first login bootstrap if enabled
      // #Pangea
      // if (client.encryption?.keyManager.enabled == true) {
      //   if (await client.encryption?.keyManager.isCached() == false ||
      //       await client.encryption?.crossSigning.isCached() == false ||
      //       client.isUnknownSession && !mounted) {
      //     await BootstrapDialog(client: client).show(context);
      //   }
      // }

      // Pangea#
    }
    // #Pangea
    if (mounted) {
      GoogleAnalytics.analyticsUserUpdate(client.userID);
      await pangeaController.subscriptionController.initialize();
      pangeaController.afterSyncAndFirstLoginInitialization(context);
      await pangeaController.inviteBotToExistingSpaces();
    } else {
      ErrorHandler.logError(
          m: "didn't run afterSyncAndFirstLoginInitialization because not mounted");
      // debugger(when: kDebugMode);
    }
    // Pangea#

    if (!mounted) return;
    setState(() {
      waitForFirstSync = true;
    });
  }

  void cancelAction() {
    if (selectMode == SelectMode.share) {
      setState(() => Matrix.of(context).shareContent = null);
    } else {
      setState(() => selectedRoomIds.clear());
    }
  }

  void setActiveClient(Client client) {
    VRouter.of(context).to('/rooms');
    setState(() {
      activeFilter = AppConfig.separateChatTypes
          ? ActiveFilter.messages
          : ActiveFilter.allChats;
      activeSpaceId = null;
      selectedRoomIds.clear();
      Matrix.of(context).setActiveClient(client);
    });
    _clientStream.add(client);
  }

  void setActiveBundle(String bundle) {
    VRouter.of(context).to('/rooms');
    setState(() {
      selectedRoomIds.clear();
      Matrix.of(context).activeBundle = bundle;
      if (!Matrix.of(context)
          .currentBundle!
          .any((client) => client == Matrix.of(context).client)) {
        Matrix.of(context)
            .setActiveClient(Matrix.of(context).currentBundle!.first);
      }
    });
  }

  void editBundlesForAccount(String? userId, String? activeBundle) async {
    final l10n = L10n.of(context)!;
    final client = Matrix.of(context)
        .widget
        .clients[Matrix.of(context).getClientIndexByMatrixId(userId!)];
    final action = await showConfirmationDialog<EditBundleAction>(
      context: context,
      title: L10n.of(context)!.editBundlesForAccount,
      actions: [
        AlertDialogAction(
          key: EditBundleAction.addToBundle,
          label: L10n.of(context)!.addToBundle,
        ),
        if (activeBundle != client.userID)
          AlertDialogAction(
            key: EditBundleAction.removeFromBundle,
            label: L10n.of(context)!.removeFromBundle,
          ),
      ],
    );
    if (action == null) return;
    switch (action) {
      case EditBundleAction.addToBundle:
        final bundle = await showTextInputDialog(
          context: context,
          title: l10n.bundleName,
          textFields: [DialogTextField(hintText: l10n.bundleName)],
        );
        if (bundle == null || bundle.isEmpty || bundle.single.isEmpty) return;
        await showFutureLoadingDialog(
          context: context,
          future: () => client.setAccountBundle(bundle.single),
        );
        break;
      case EditBundleAction.removeFromBundle:
        await showFutureLoadingDialog(
          context: context,
          future: () => client.removeFromAccountBundle(activeBundle!),
        );
    }
  }

  bool get displayBundles =>
      Matrix.of(context).hasComplexBundles &&
      Matrix.of(context).accountBundles.keys.length > 1;

  String? get secureActiveBundle {
    if (Matrix.of(context).activeBundle == null ||
        !Matrix.of(context)
            .accountBundles
            .keys
            .contains(Matrix.of(context).activeBundle)) {
      return Matrix.of(context).accountBundles.keys.first;
    }
    return Matrix.of(context).activeBundle;
  }

  void resetActiveBundle() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        Matrix.of(context).activeBundle = null;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Matrix.of(context).navigatorContext = context;
    return ChatListView(this);
  }

  void _hackyWebRTCFixForWeb() {
    ChatList.contextForVoip = context;
  }

  Future<void> _checkTorBrowser() async {
    if (!kIsWeb) return;
    final isTor = await TorBrowserDetector.isTorBrowser;
    isTorBrowser = isTor;
  }

  Future<void> dehydrate() =>
      SettingsSecurityController.dehydrateDevice(context);
}

enum EditBundleAction { addToBundle, removeFromBundle }
