import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vrouter/vrouter.dart';

import '../../config/themes.dart';
import 'chat_list.dart';

class StartChatFloatingActionButton extends StatelessWidget {
  final ActiveFilter activeFilter;
  final ValueNotifier<bool> scrolledToTop;
  final bool roomsIsEmpty;
  // //#Pangea
  final ChatListController controller;
  // //Pangea#

  const StartChatFloatingActionButton({
    Key? key,
    required this.activeFilter,
    required this.scrolledToTop,
    required this.roomsIsEmpty,
    // //#Pangea
    required this.controller,
    // //Pangea#
  }) : super(key: key);

  void _onPressed(BuildContext context) {
    //#Pangea
    if (controller.activeSpaceId != null) {
      VRouter.of(context).to('/newgroup/${controller.activeSpaceId}');
      return;
    }
    //Pangea#
    switch (activeFilter) {
      case ActiveFilter.allChats:
      case ActiveFilter.messages:
      //#Pangea
      // VRouter.of(context).to('/newprivatechat');
      // break;
      //Pangea#
      case ActiveFilter.groups:
        //#Pangea
        VRouter.of(context).to('/newgroup/${controller.activeSpaceId}');
        // VRouter.of(context).to('/newgroup');
        //Pangea#
        break;
      case ActiveFilter.spaces:
        VRouter.of(context).to('/newspace');
    }
  }

  IconData get icon {
    //#Pangea
    if (controller.activeSpaceId != null) {
      return Icons.group_add_outlined;
    }
    //Pangea#
    switch (activeFilter) {
      case ActiveFilter.allChats:
      case ActiveFilter.messages:
      //#Pangea
      // return Icons.add_outlined;
      //Pangea#
      case ActiveFilter.groups:
        return Icons.group_add_outlined;
      case ActiveFilter.spaces:
        return Icons.workspaces_outlined;
    }
    //Pangea#
  }

  String getLabel(BuildContext context) {
    // //#Pangea
    if (controller.activeSpaceId != null) {
      return L10n.of(context)!.newGroup;
    }
    //Pangea#
    switch (activeFilter) {
      case ActiveFilter.allChats:
      case ActiveFilter.messages:
        return roomsIsEmpty
            ? L10n.of(context)!.startFirstChat
            : L10n.of(context)!.newChat;
      case ActiveFilter.groups:
        return L10n.of(context)!.newGroup;
      case ActiveFilter.spaces:
        return L10n.of(context)!.newSpace;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: scrolledToTop,
      builder: (context, scrolledToTop, _) => AnimatedContainer(
        duration: FluffyThemes.animationDuration,
        curve: FluffyThemes.animationCurve,
        width: roomsIsEmpty
            ? null
            : scrolledToTop
                //#Pangea
                // ? 144
                ? null
                // : 56
                : null,
        //Pangea#
        child: scrolledToTop
            ? FloatingActionButton.extended(
                onPressed: () => _onPressed(context),
                icon: Icon(icon),
                label: Text(
                  getLabel(context),
                  overflow: TextOverflow.fade,
                ),
              )
            : FloatingActionButton(
                onPressed: () => _onPressed(context),
                child: Icon(icon),
              ),
      ),
    );
  }
}
