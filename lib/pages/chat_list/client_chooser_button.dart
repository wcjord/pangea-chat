import 'dart:developer';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:fluffychat/pangea/utils/find_conversation_partner_dialog.dart';
import 'package:fluffychat/pangea/utils/logout.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:keyboard_shortcuts/keyboard_shortcuts.dart';
import 'package:matrix/matrix.dart';
import 'package:vrouter/vrouter.dart';

import '../../pangea/extensions/client_extension.dart';
import '../../pangea/utils/class_code.dart';
import '../../utils/fluffy_share.dart';
import 'chat_list.dart';

class ClientChooserButton extends StatelessWidget {
  final ChatListController controller;

  const ClientChooserButton(this.controller, {Key? key}) : super(key: key);

  List<PopupMenuEntry<Object>> _bundleMenuItems(BuildContext context) {
    final matrix = Matrix.of(context);
    final bundles = matrix.accountBundles.keys.toList()
      ..sort(
        (a, b) => a!.isValidMatrixId == b!.isValidMatrixId
            ? 0
            : a.isValidMatrixId && !b.isValidMatrixId
                ? -1
                : 1,
      );

    return <PopupMenuEntry<Object>>[
      // #Pangea
      // PopupMenuItem(
      //   value: SettingsAction.newStory,
      //   child: Row(
      //     children: [
      //       const Icon(Icons.camera_outlined),
      //       const SizedBox(width: 18),
      //       Text(L10n.of(context)!.yourStory),
      //     ],
      //   ),
      // ),
      // PopupMenuItem(
      //   value: SettingsAction.newGroup,
      //   child: Row(
      //     children: [
      //       const Icon(Icons.group_add_outlined),
      //       const SizedBox(width: 18),
      //       Text(L10n.of(context)!.createNewGroup),
      //     ],
      //   ),
      // ),
      PopupMenuItem(
        value: SettingsAction.joinWithClassCode,
        child: Row(
          children: [
            const Icon(Icons.join_full_outlined),
            const SizedBox(width: 18),
            Expanded(child: Text(L10n.of(context)!.joinWithClassCode)),
          ],
        ),
      ),
      PopupMenuItem(
        enabled: matrix.client.classesAndExchangesImTeaching.isNotEmpty,
        value: SettingsAction.classAnalytics,
        child: Row(
          children: [
            const Icon(Icons.analytics_outlined),
            const SizedBox(width: 18),
            Expanded(child: Text(L10n.of(context)!.classAnalytics)),
          ],
        ),
      ),
      PopupMenuItem(
        enabled: matrix.client.classesImIn.isNotEmpty,
        value: SettingsAction.myAnalytics,
        child: Row(
          children: [
            const Icon(Icons.analytics_outlined),
            const SizedBox(width: 18),
            Expanded(child: Text(L10n.of(context)!.myLearning)),
          ],
        ),
      ),
      PopupMenuItem(
        //#Pangea
        // value: SettingsAction.newSpace,
        value: SettingsAction.newClass,
        //Pangea#
        child: Row(
          children: [
            //#Pangea
            // const Icon(Icons.workspaces_outlined),
            const Icon(Icons.school),
            //Pangea#
            const SizedBox(width: 18),
            // #Pangea
            // Text(L10n.of(context)!.createNewSpace),
            Expanded(child: Text(L10n.of(context)!.createNewClass)),
            //Pangea#
          ],
        ),
      ),
      // #Pangea
      PopupMenuItem(
        value: SettingsAction.newExchange,
        child: Row(
          children: [
            const Icon(Icons.connecting_airports),
            const SizedBox(width: 18),
            Expanded(child: Text(L10n.of(context)!.newExchange)),
          ],
        ),
      ),
      PopupMenuItem(
        value: SettingsAction.findAClass,
        enabled: false,
        child: Row(
          children: [
            const Icon(Icons.class_outlined),
            const SizedBox(width: 18),
            Expanded(child: Text(L10n.of(context)!.findAClass)),
          ],
        ),
      ),
      if (controller.pangeaController.permissionsController.isUser18())
        PopupMenuItem(
          value: SettingsAction.findAConversationPartner,
          child: Row(
            children: [
              const Icon(Icons.add_circle_outline),
              const SizedBox(width: 18),
              Expanded(child: Text(L10n.of(context)!.findALanguagePartner)),
            ],
          ),
        ),
      // PopupMenuItem(
      //   value: SettingsAction.invite,
      //   child: Row(
      //     children: [
      //       Icon(Icons.adaptive.share_outlined),
      //       const SizedBox(width: 18),
      //       Text(L10n.of(context)!.inviteContact),
      //     ],
      //   ),
      // ),
      // Pangea#
      PopupMenuItem(
        value: SettingsAction.archive,
        child: Row(
          children: [
            const Icon(Icons.archive_outlined),
            const SizedBox(width: 18),
            // #Pangea
            // Text(L10n.of(context)!.archive),
            Expanded(child: Text(L10n.of(context)!.archive)),
            // Pangea#
          ],
        ),
      ),
      PopupMenuItem(
        value: SettingsAction.settings,
        child: Row(
          children: [
            const Icon(Icons.settings_outlined),
            const SizedBox(width: 18),
            // #Pangea
            // Text(L10n.of(context)!.settings),
            Expanded(child: Text(L10n.of(context)!.settings)),
            // Pangea#
          ],
        ),
      ),
      // #Pangea
      // const PopupMenuItem(
      //   value: null,
      //   child: Divider(height: 1),
      // ),
      // for (final bundle in bundles) ...[
      //   if (matrix.accountBundles[bundle]!.length != 1 ||
      //       matrix.accountBundles[bundle]!.single!.userID != bundle)
      //     PopupMenuItem(
      //       value: null,
      //       child: Column(
      //         crossAxisAlignment: CrossAxisAlignment.start,
      //         mainAxisSize: MainAxisSize.min,
      //         children: [
      //           Text(
      //             bundle!,
      //             style: TextStyle(
      //               color: Theme.of(context).textTheme.titleMedium!.color,
      //               fontSize: 14,
      //             ),
      //           ),
      //           const Divider(height: 1),
      //         ],
      //       ),
      //     ),
      //   ...matrix.accountBundles[bundle]!
      //       .map(
      //         (client) => PopupMenuItem(
      //           value: client,
      //           child: FutureBuilder<Profile?>(
      //             // analyzer does not understand this type cast for error
      //             // handling
      //             //
      //             // ignore: unnecessary_cast
      //             future: (client!.fetchOwnProfile() as Future<Profile?>)
      //                 .onError((e, s) => null),
      //             builder: (context, snapshot) => Row(
      //               children: [
      //                 Avatar(
      //                   mxContent: snapshot.data?.avatarUrl,
      //                   name: snapshot.data?.displayName ??
      //                       client.userID!.localpart,
      //                   size: 32,
      //                   fontSize: 12,
      //                 ),
      //                 const SizedBox(width: 12),
      //                 Expanded(
      //                   child: Text(
      //                     snapshot.data?.displayName ??
      //                         client.userID!.localpart!,
      //                     overflow: TextOverflow.ellipsis,
      //                   ),
      //                 ),
      //                 const SizedBox(width: 12),
      //                 IconButton(
      //                   icon: const Icon(Icons.edit_outlined),
      //                   onPressed: () => controller.editBundlesForAccount(
      //                     client.userID,
      //                     bundle,
      //                   ),
      //                 ),
      //               ],
      //             ),
      //           ),
      //         ),
      //       )
      //       .toList(),
      // ],
      // PopupMenuItem(
      //   value: SettingsAction.addAccount,
      //   child: Row(
      //     children: [
      //       const Icon(Icons.person_add_outlined),
      //       const SizedBox(width: 18),
      //       Text(L10n.of(context)!.addAccount),
      //     ],
      //   ),
      // ),
      PopupMenuItem(
        value: SettingsAction.logout,
        child: Row(
          children: [
            const Icon(Icons.logout_outlined),
            const SizedBox(width: 18),
            Expanded(child: Text(L10n.of(context)!.logout)),
          ],
        ),
      ),
      // Pangea#
    ];
  }

  @override
  Widget build(BuildContext context) {
    final matrix = Matrix.of(context);

    int clientCount = 0;
    matrix.accountBundles.forEach((key, value) => clientCount += value.length);
    return FutureBuilder<Profile>(
      future: matrix.client.fetchOwnProfile(),
      builder: (context, snapshot) => Stack(
        alignment: Alignment.center,
        children: [
          ...List.generate(
            clientCount,
            (index) => KeyBoardShortcuts(
              keysToPress: _buildKeyboardShortcut(index + 1),
              helpLabel: L10n.of(context)!.switchToAccount(index + 1),
              onKeysPressed: () => _handleKeyboardShortcut(
                matrix,
                index,
                context,
              ),
              child: const SizedBox.shrink(),
            ),
          ),
          KeyBoardShortcuts(
            keysToPress: {
              LogicalKeyboardKey.controlLeft,
              LogicalKeyboardKey.tab
            },
            helpLabel: L10n.of(context)!.nextAccount,
            onKeysPressed: () => _nextAccount(matrix, context),
            child: const SizedBox.shrink(),
          ),
          KeyBoardShortcuts(
            keysToPress: {
              LogicalKeyboardKey.controlLeft,
              LogicalKeyboardKey.shiftLeft,
              LogicalKeyboardKey.tab
            },
            helpLabel: L10n.of(context)!.previousAccount,
            onKeysPressed: () => _previousAccount(matrix, context),
            child: const SizedBox.shrink(),
          ),
          PopupMenuButton<Object>(
            onSelected: (o) => _clientSelected(o, context),
            itemBuilder: _bundleMenuItems,
            //#Pangea
            child: Material(
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(12),
                bottomRight: Radius.circular(12),
              ),
              clipBehavior: Clip.hardEdge,
              child: ListTile(
                tileColor: Theme.of(context).scaffoldBackgroundColor,
                hoverColor: Theme.of(context).colorScheme.onSurfaceVariant,
                leading: const Icon(Icons.settings_outlined),
                title: Text(L10n.of(context)!.mainMenu),
              ),
            ),
            // child: Material(
            //   color: Colors.transparent,
            //   borderRadius: BorderRadius.circular(99),
            //   child: Avatar(
            //     mxContent: snapshot.data?.avatarUrl,
            //     name: snapshot.data?.displayName ??
            //         matrix.client.userID!.localpart,
            //     size: 28,
            //     fontSize: 12,
            //   ),
            // ),
            //Pangea#
          ),
        ],
      ),
    );
  }

  Set<LogicalKeyboardKey>? _buildKeyboardShortcut(int index) {
    if (index > 0 && index < 10) {
      return {
        LogicalKeyboardKey.altLeft,
        LogicalKeyboardKey(0x00000000030 + index)
      };
    } else {
      return null;
    }
  }

  void _clientSelected(
    Object object,
    BuildContext context,
  ) async {
    if (object is Client) {
      controller.setActiveClient(object);
    } else if (object is String) {
      controller.setActiveBundle(object);
    } else if (object is SettingsAction) {
      switch (object) {
        case SettingsAction.addAccount:
          final consent = await showOkCancelAlertDialog(
            context: context,
            title: L10n.of(context)!.addAccount,
            message: L10n.of(context)!.enableMultiAccounts,
            okLabel: L10n.of(context)!.next,
            cancelLabel: L10n.of(context)!.cancel,
          );
          if (consent != OkCancelResult.ok) return;
          VRouter.of(context).to('/settings/addaccount');
          break;
        case SettingsAction.newStory:
          VRouter.of(context).to('/stories/create');
          break;
        case SettingsAction.newGroup:
          // #Pangea
          VRouter.of(context).to('/newgroup/${controller.activeSpaceId}');
          // VRouter.of(context).to('/newgroup');
          //Pangea#
          break;
        case SettingsAction.newSpace:
          VRouter.of(context).to('/newspace');
          break;
        case SettingsAction.invite:
          FluffyShare.share(
            L10n.of(context)!.inviteText(
              Matrix.of(context).client.userID!,
              'https://matrix.to/#/${Matrix.of(context).client.userID}?client=im.fluffychat',
            ),
            context,
          );
          break;
        case SettingsAction.settings:
          VRouter.of(context).to('/settings');
          break;
        case SettingsAction.archive:
          VRouter.of(context).to('/archive');
          break;
        // #Pangea
        case SettingsAction.newClass:
          //send to newspace with vrouter path parameter newclass == true
          VRouter.of(context).to('/newspace');
          break;
        case SettingsAction.newExchange:
          VRouter.of(context).to('/newspace/exchange');
          break;
        case SettingsAction.joinWithClassCode:
          ClassCodeUtil.joinWithClassCodeDialog(
              context, controller.pangeaController, null);
          break;
        case SettingsAction.findAConversationPartner:
          findConversationPartnerDialog(
            context,
            controller.pangeaController,
          );
          break;
        case SettingsAction.classAnalytics:
          VRouter.of(context).to('/analytics');
          break;
        case SettingsAction.myAnalytics:
          VRouter.of(context).to('/mylearning');
          break;
        case SettingsAction.findAClass:
          debugger(when: kDebugMode, message: "left to implement");
          break;
        case SettingsAction.logout:
          pLogoutAction(context);
          break;
        // #Pangea
      }
    }
  }

  void _handleKeyboardShortcut(
    MatrixState matrix,
    int index,
    BuildContext context,
  ) {
    final bundles = matrix.accountBundles.keys.toList()
      ..sort(
        (a, b) => a!.isValidMatrixId == b!.isValidMatrixId
            ? 0
            : a.isValidMatrixId && !b.isValidMatrixId
                ? -1
                : 1,
      );
    // beginning from end if negative
    if (index < 0) {
      int clientCount = 0;
      matrix.accountBundles
          .forEach((key, value) => clientCount += value.length);
      _handleKeyboardShortcut(matrix, clientCount, context);
    }
    for (final bundleName in bundles) {
      final bundle = matrix.accountBundles[bundleName];
      if (bundle != null) {
        if (index < bundle.length) {
          return _clientSelected(bundle[index]!, context);
        } else {
          index -= bundle.length;
        }
      }
    }
    // if index too high, restarting from 0
    _handleKeyboardShortcut(matrix, 0, context);
  }

  int? _shortcutIndexOfClient(MatrixState matrix, Client client) {
    int index = 0;

    final bundles = matrix.accountBundles.keys.toList()
      ..sort(
        (a, b) => a!.isValidMatrixId == b!.isValidMatrixId
            ? 0
            : a.isValidMatrixId && !b.isValidMatrixId
                ? -1
                : 1,
      );
    for (final bundleName in bundles) {
      final bundle = matrix.accountBundles[bundleName];
      if (bundle == null) return null;
      if (bundle.contains(client)) {
        return index + bundle.indexOf(client);
      } else {
        index += bundle.length;
      }
    }
    return null;
  }

  void _nextAccount(MatrixState matrix, BuildContext context) {
    final client = matrix.client;
    final lastIndex = _shortcutIndexOfClient(matrix, client);
    _handleKeyboardShortcut(matrix, lastIndex! + 1, context);
  }

  void _previousAccount(MatrixState matrix, BuildContext context) {
    final client = matrix.client;
    final lastIndex = _shortcutIndexOfClient(matrix, client);
    _handleKeyboardShortcut(matrix, lastIndex! - 1, context);
  }
}

enum SettingsAction {
  addAccount,
  newStory,
  newGroup,
  newSpace,
  invite,
  settings,
  archive,
  // #Pangea
  joinWithClassCode,
  classAnalytics,
  myAnalytics,
  findAClass,
  findAConversationPartner,
  logout,
  newClass,
  newExchange
  // #Pangea
}
