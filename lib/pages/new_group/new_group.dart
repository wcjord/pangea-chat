import 'dart:developer';

import 'package:fluffychat/pages/new_group/new_group_view.dart';
import 'package:fluffychat/pangea/controllers/pangea_controller.dart';
import 'package:fluffychat/pangea/models/chat_topic_model.dart';
import 'package:fluffychat/pangea/utils/class_chat_power_levels.dart';
import 'package:fluffychat/pangea/utils/error_handler.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart' as sdk;
import 'package:vrouter/vrouter.dart';

import '../../pangea/models/lemma.dart';
import '../../pangea/utils/firebase_analytics.dart';
import '../../pangea/widgets/class/add_space_toggles.dart';

class NewGroup extends StatefulWidget {
  const NewGroup({Key? key}) : super(key: key);

  @override
  NewGroupController createState() => NewGroupController();
}

class NewGroupController extends State<NewGroup> {
  TextEditingController controller = TextEditingController();
  bool publicGroup = false;

  //#Pangea
  PangeaController pangeaController = MatrixState.pangeaController;
  final GlobalKey<AddToSpaceState> addToSpaceKey = GlobalKey<AddToSpaceState>();

  ChatTopic chatTopic = ChatTopic.empty;

  void setVocab(List<Lemma> vocab) => setState(() => chatTopic.vocab = vocab);

  String? get activeSpaceId => VRouter.of(context).pathParameters['spaceid'];
  // Pangea#

  void setPublicGroup(bool b) => setState(() => publicGroup = b);

  void submitAction([_]) async {
    // #Pangea
    if (controller.text.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(L10n.of(context)!.emptyChatNameWarning),
        ),
      );
      return;
    }
    // Pangea#
    final client = Matrix.of(context).client;
    final roomID = await showFutureLoadingDialog(
      context: context,
      future: () async {
        final roomId = await client.createGroupChat(
          // #Pangea
          // visibility:
          //     publicGroup ? sdk.Visibility.public : sdk.Visibility.private,
          // preset: publicGroup
          //     ? sdk.CreateRoomPreset.publicChat
          //     : sdk.CreateRoomPreset.privateChat,
          preset: sdk.CreateRoomPreset.publicChat,
          // initialState: [
          //   StateEvent(
          //       content: {'topic': chatTopic.description},
          //       type: EventTypes.RoomTopic),
          //   StateEvent(
          //       content: chatTopic.toJson(), type: PangeaEventTypes.roomInfo),
          // ],
          powerLevelContentOverride:
              await ClassChatPowerLevels.powerLevelOverrideForClassChat(
            context,
            addToSpaceKey.currentState!.parents
                .map((suggestionStatus) => suggestionStatus.room)
                .toList(),
          ),
          // Pangea#
          groupName: controller.text,
        );
        return roomId;
      },
      onError: (exception) {
        ErrorHandler.logError(e: exception, s: StackTrace.current);
        return exception.toString();
      },
    );
    if (roomID.error == null) {
      //#Pangea
      GoogleAnalytics.createChat(roomID.result!);
      await addToSpaceKey.currentState!.addSpaces(roomID.result!);
      //Pangea#

      VRouter.of(context).toSegments(['rooms', roomID.result!, 'invite']);

      //#Pangea
    } else {
      debugger(when: kDebugMode);
      ErrorHandler.logError(e: roomID.error, s: StackTrace.current);
    }
    //Pangea#
  }

  //#Pangea
  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      chatTopic.langCode =
          pangeaController.languageController.activeL2Code(roomID: null) ??
              pangeaController.pLanguageStore.targetOptions.first.langCode;
      setState(() {});
    });

    super.initState();
  }
  //Pangea#

  @override
  Widget build(BuildContext context) => NewGroupView(this);
}
