import 'dart:typed_data';

import 'package:fluffychat/config/app_config.dart';
import 'package:fluffychat/pages/connect/connect_page.dart';
import 'package:fluffychat/pangea/pages/connect/p_sso_button.dart';
import 'package:fluffychat/pangea/widgets/common/pangea_logo_svg.dart';
import 'package:fluffychat/widgets/layouts/login_scaffold.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class ConnectPageView extends StatelessWidget {
  final ConnectPageController controller;
  const ConnectPageView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final avatar = Matrix.of(context).loginAvatar;
    final identityProviders = controller.identityProviders;
    return LoginScaffold(
      appBar: AppBar(
        leading: controller.loading ? null : const BackButton(),
        automaticallyImplyLeading: !controller.loading,
        centerTitle: true,
        title: Text(
          // #Pangea //functionality not required
          // Matrix.of(context).getLoginClient().homeserver?.host ?? '',
          AppConfig.applicationName,
          style: const TextStyle(color: Colors.white),
          // #Pangea
        ),
      ),
      body: ListView(
        key: const Key('ConnectPageListView'),
        children: [
          if (Matrix.of(context).loginRegistrationSupported ?? false) ...[
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Center(
                child: Stack(
                  children: [
                    Material(
                      borderRadius: BorderRadius.circular(64),
                      elevation: Theme.of(context)
                              .appBarTheme
                              .scrolledUnderElevation ??
                          10,
                      color: Colors.transparent,
                      shadowColor: Theme.of(context)
                          .colorScheme
                          .onBackground
                          .withAlpha(64),
                      clipBehavior: Clip.hardEdge,
                      child: CircleAvatar(
                        radius: 64,
                        backgroundColor: Colors.white,
                        child: avatar == null
                            ? const Icon(
                                // #Pangea
                                // Icons.person,
                                Icons.person_outlined,
                                // #Pangea
                                color: Colors.black,
                                size: 64,
                              )
                            : FutureBuilder<Uint8List>(
                                future: avatar.readAsBytes(),
                                builder: (context, snapshot) {
                                  final bytes = snapshot.data;
                                  if (bytes == null) {
                                    return const CircularProgressIndicator
                                        .adaptive();
                                  }
                                  return Image.memory(
                                    bytes,
                                    fit: BoxFit.cover,
                                    width: 128,
                                    height: 128,
                                  );
                                },
                              ),
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: FloatingActionButton(
                        mini: true,
                        onPressed: controller.pickAvatar,
                        backgroundColor: Colors.white,
                        foregroundColor: Colors.black,
                        child: const Icon(Icons.camera_alt_outlined),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: TextField(
                controller: controller.usernameController,
                onSubmitted: (_) => controller.signUp(),
                decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.account_box_outlined),
                  hintText: L10n.of(context)!.chooseAUsername,
                  errorText: controller.signupError,
                  errorStyle: const TextStyle(color: Colors.orange),
                  // #Pangea
                  fillColor: Theme.of(context)
                      .colorScheme
                      .background
                      .withOpacity(0.75),
                  // #Pangea
                ),
              ),
            ),
            // #Pangea
            // Padding(
            //   padding: const EdgeInsets.all(12.0),
            //   child: Hero(
            //     tag: 'loginButton',
            //     child: ElevatedButton.icon(
            //       style: ElevatedButton.styleFrom(
            //         backgroundColor: Theme.of(context).colorScheme.primary,
            //         foregroundColor: Theme.of(context).colorScheme.onPrimary,
            //       ),
            //       onPressed: controller.loading ? () {} : controller.signUp,
            //       icon: const Icon(Icons.person_add_outlined),
            //       label: controller.loading
            //           ? const LinearProgressIndicator()
            //           : Text(L10n.of(context)!.signUp),
            //     ),
            //   ),
            // ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Hero(
                tag: 'signUpButton',
                child: ElevatedButton(
                  onPressed: controller.loading ? () {} : controller.signUp,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const PangeaLogoSvg(width: 20),
                      Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Text(L10n.of(context)!.signUp),
                      )
                    ],
                  ),
                ),
              ),
            ),
            // #Pangea
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                children: [
                  const Expanded(
                    child: Divider(
                      thickness: 1,
                      // #Pangea
                      // color: Theme.of(context).dividerColor,
                      color: Colors.white,
                      // #Pangea
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Text(
                      L10n.of(context)!.or,
                      style: const TextStyle(
                        // #Pangea
                        color: Colors.white,
                        // #Pangea
                        fontSize: 18,
                      ),
                    ),
                  ),
                  const Expanded(
                    child: Divider(
                      thickness: 1,
                      // #Pangea
                      // color: Theme.of(context).dividerColor,
                      color: Colors.white,
                      // #Pangea
                    ),
                  ),
                ],
              ),
            ),
          ],
          if (controller.supportsSso)
            identityProviders == null
                ? const SizedBox(
                    height: 74,
                    child: Center(
                      child: CircularProgressIndicator.adaptive(
                        // #Pangea
                        backgroundColor: Colors.white,
                        // #Pangea
                      ),
                    ),
                  )
                // #Pangea
                // : Center(
                //     child: identityProviders.length == 1
                //         ? Container(
                //             width: double.infinity,
                //             padding: const EdgeInsets.all(12.0),
                //             child: ElevatedButton.icon(
                //               style: ElevatedButton.styleFrom(
                //                 backgroundColor: Theme.of(context)
                //                     .colorScheme
                //                     .primaryContainer,
                //                 foregroundColor: Theme.of(context)
                //                     .colorScheme
                //                     .onPrimaryContainer,
                //               ),
                //               icon: identityProviders.single.icon == null
                //                   ? const Icon(
                //                       Icons.web_outlined,
                //                       size: 16,
                //                     )
                //                   : Image.network(
                //                       Uri.parse(identityProviders.single.icon!)
                //                           .getDownloadLink(
                //                             Matrix.of(context).getLoginClient(),
                //                           )
                //                           .toString(),
                //                       width: 32,
                //                       height: 32,
                //                     ),
                //               onPressed: () => controller
                //                   .ssoLoginAction(identityProviders.single.id!),
                //               label: Text(
                //                 identityProviders.single.name ??
                //                     identityProviders.single.brand ??
                //                     L10n.of(context)!.loginWithOneClick,
                //               ),
                //             ),
                //           )
                //         : Wrap(
                //             children: [
                //               for (final identityProvider in identityProviders)
                //                 SsoButton(
                //                   onPressed: () => controller
                //                       .ssoLoginAction(identityProvider.id!),
                //                   identityProvider: identityProvider,
                //                 ),
                //             ].toList(),
                //           ),
                //   ),
                : Column(
                    children: [
                      ...identityProviders.reversed
                          .map(
                            (provider) => Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Hero(
                                tag:
                                    "ssobutton ${provider.id ?? provider.name}",
                                child: PangeaSsoButton(
                                  onPressed: () =>
                                      controller.ssoLoginAction(provider),
                                  identityProvider: provider,
                                ),
                              ),
                            ),
                          )
                          .toList(),
                    ],
                  ),
          // #Pangea
          if (controller.supportsLogin)
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Hero(
                tag: 'signinButton',
                // #Pangea
                // child: ElevatedButton.icon(
                //   icon: const Icon(Icons.login_outlined),
                //   style: ElevatedButton.styleFrom(
                //     backgroundColor:
                //         Theme.of(context).colorScheme.primaryContainer,
                //     foregroundColor:
                //         Theme.of(context).colorScheme.onPrimaryContainer,
                //   ),
                //   label: Text(L10n.of(context)!.login),
                //   onPressed: controller.loading ? () {} : controller.login,
                // ),
                child: ElevatedButton(
                  onPressed: controller.loading ? () {} : controller.login,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const PangeaLogoSvg(width: 20),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Text(
                            "${L10n.of(context)!.loginOrSignup} Pangea Chat"),
                      )
                    ],
                  ),
                ),
                // #Pangea
              ),
            ),
        ],
      ),
    );
  }
}
