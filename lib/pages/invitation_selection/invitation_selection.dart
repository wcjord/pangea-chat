import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:fluffychat/pages/invitation_selection/invitation_selection_view.dart';
import 'package:fluffychat/pangea/extensions/pangea_room_extension.dart';
import 'package:fluffychat/pangea/utils/bot_name.dart';
import 'package:fluffychat/pangea/utils/error_handler.dart';
import 'package:fluffychat/utils/matrix_sdk_extensions/matrix_locals.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart';
import 'package:vrouter/vrouter.dart';

import '../../pangea/constants/class_default_values.dart';
import '../../utils/localized_exception_extension.dart';

//#Pangea
enum InvitationSelectionMode { admin, member }
//Pangea#

class InvitationSelection extends StatefulWidget {
  const InvitationSelection({Key? key}) : super(key: key);

  @override
  InvitationSelectionController createState() =>
      InvitationSelectionController();
}

class InvitationSelectionController extends State<InvitationSelection> {
  TextEditingController controller = TextEditingController();
  late String currentSearchTerm;
  bool loading = false;
  List<Profile> foundProfiles = [];
  Timer? coolDown;

  String? get roomId => VRouter.of(context).pathParameters['roomid'];

  Future<List<User>> getContacts(BuildContext context) async {
    final client = Matrix.of(context).client;
    final room = client.getRoomById(roomId!)!;
    final participants = await room.requestParticipants();
    participants.removeWhere(
      (u) => ![Membership.join, Membership.invite].contains(u.membership),
    );
    final participantsIds = participants.map((p) => p.stateKey).toList();
    final contacts = client.rooms
        .where((r) => r.isDirectChat)
        .map((r) => r.unsafeGetUserFromMemoryOrFallback(r.directChatMatrixID!))
        .toList()
      ..removeWhere((u) => participantsIds.contains(u.stateKey));
    contacts.sort(
      (a, b) => a.calcDisplayname().toLowerCase().compareTo(
            b.calcDisplayname().toLowerCase(),
          ),
    );
    //#Pangea
    // return contacts;
    return contacts.where((u) => u.id != BotName.byEnvironment).toList();
    //Pangea#
  }

  //#Pangea
  // add all students (already local) from spaceParents who aren't already in room to eligibleStudents
  // use room.members to get all users in room
  bool _initialized = false;
  Future<List<User>> eligibleStudents(
    BuildContext context,
    String text,
  ) async {
    if (!_initialized) {
      _initialized = true;
      await requestParentSpaceParticipants();
    }

    final eligibleStudents = <User>[];
    final spaceParents = room.pangeaSpaceParents;
    final userId = Matrix.of(context).client.userID;
    for (final Room space in spaceParents) {
      eligibleStudents.addAll(
        space.getParticipants().where(
              (spaceUser) =>
                  spaceUser.id != BotName.byEnvironment &&
                  spaceUser.id != "@support:staging.pangea.chat" &&
                  spaceUser.id != userId &&
                  (text.isEmpty ||
                      (spaceUser.displayName
                              ?.toLowerCase()
                              .contains(text.toLowerCase()) ??
                          false) ||
                      spaceUser.id.toLowerCase().contains(text.toLowerCase())),
            ),
      );
    }
    return eligibleStudents;
  }

  Future<SearchUserDirectoryResponse>
      eligibleStudentsAsSearchUserDirectoryResponse(
    BuildContext context,
    String text,
  ) async {
    return SearchUserDirectoryResponse(
      results: (await eligibleStudents(context, text))
          .map((e) => Profile(
              userId: e.id, avatarUrl: e.avatarUrl, displayName: e.displayName))
          .toList(),
      limited: false,
    );
  }

  List<User> studentsInRoom(BuildContext context) => room
      .getParticipants()
      .where(
        (u) => [Membership.join, Membership.invite].contains(u.membership),
      )
      .toList();
  //Pangea#

  void inviteAction(BuildContext context, String id) async {
    final room = Matrix.of(context).client.getRoomById(roomId!)!;
    if (OkCancelResult.ok !=
        await showOkCancelAlertDialog(
          context: context,
          title: L10n.of(context)!.inviteContactToGroup(
            room.getLocalizedDisplayname(
              MatrixLocals(L10n.of(context)!),
            ),
          ),
          okLabel: L10n.of(context)!.yes,
          cancelLabel: L10n.of(context)!.cancel,
        )) {
      return;
    }
    final success = await showFutureLoadingDialog(
      context: context,
      //#Pangea
      // future: () => room.invite(id),
      future: () => Future.wait([
        room.invite(id),
        room.setPower(id, ClassDefaultValues.powerLevelOfAdmin),
        if (room.isSpace)
          ...room.spaceChildren
              .map(
                (e) => roomId != null
                    ? Matrix.of(context).client.getRoomById(e.roomId!)
                    : null,
              )
              .where((element) => element != null)
              .cast<Room>()
              .map(
                (e) => Future.wait([
                  e.invite(id),
                  e.setPower(id, ClassDefaultValues.powerLevelOfAdmin),
                ]),
              ),
      ]),
      onError: (e) => ErrorHandler.logError(e: e, s: StackTrace.current),
    );
    if (success.error == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(L10n.of(context)!.contactHasBeenInvitedToTheGroup),
        ),
      );
    }
  }

  void searchUserWithCoolDown(String text) async {
    coolDown?.cancel();
    coolDown = Timer(
      const Duration(milliseconds: 500),
      () => searchUser(context, text),
    );
  }

  void searchUser(BuildContext context, String text) async {
    coolDown?.cancel();
    if (text.isEmpty) {
      setState(() => foundProfiles = []);
    }
    currentSearchTerm = text;
    if (currentSearchTerm.isEmpty) return;
    if (loading) return;
    setState(() => loading = true);
    final matrix = Matrix.of(context);
    SearchUserDirectoryResponse response;
    try {
      //#Pangea
      // response = await matrix.client.searchUserDirectory(text, limit: 10);
      response = await (mode == InvitationSelectionMode.admin
          ? matrix.client.searchUserDirectory(text, limit: 10)
          : eligibleStudentsAsSearchUserDirectoryResponse(
              context,
              text,
            ));
      //Pangea#
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text((e).toLocalizedString(context))),
      );
      return;
    } finally {
      setState(() => loading = false);
    }
    setState(() {
      foundProfiles = List<Profile>.from(response.results);
      if (text.isValidMatrixId &&
          foundProfiles.indexWhere((profile) => text == profile.userId) == -1) {
        setState(
          () => foundProfiles = [
            Profile.fromJson({'user_id': text}),
          ],
        );
      }
      final participants = Matrix.of(context)
          .client
          .getRoomById(roomId!)!
          .getParticipants()
          .where(
            (user) =>
                [Membership.join, Membership.invite].contains(user.membership),
          )
          .toList();
      foundProfiles.removeWhere(
        (profile) =>
            //#Pangea
            // participants.indexWhere((u) => u.id == profile.userId) != -1,
            participants.indexWhere((u) => u.id == profile.userId) != -1 &&
            BotName.byEnvironment != profile.userId,
        //Pangea#
      );
    });
  }

  //#Pangea
  Room? _room;
  Room get room => _room ??= Matrix.of(context).client.getRoomById(roomId!)!;

  // request participants for all parent spaces
  Future<void> requestParentSpaceParticipants() async {
    final spaceParents = room.pangeaSpaceParents;
    await Future.wait([
      ...spaceParents.map((r) async {
        await r.requestParticipants();
      }),
      room.requestParticipants(),
    ]);
  }

  InvitationSelectionMode mode = InvitationSelectionMode.member;

  StreamSubscription<SyncUpdate>? _spaceSubscription;
  @override
  void initState() {
    Future.delayed(
      Duration.zero,
      () => setState(
        () => mode = room.isSpace
            ? InvitationSelectionMode.admin
            : InvitationSelectionMode.member,
      ),
    );
    _spaceSubscription = Matrix.of(context)
        .client
        .onSync
        .stream
        .where((event) =>
            event.rooms?.join?.keys.any((ithRoomId) =>
                room.pangeaSpaceParents.map((e) => e.id).contains(ithRoomId)) ??
            false)
        .listen(
      (SyncUpdate syncUpdate) async {
        await requestParentSpaceParticipants();
        setState(() {});
      },
    );
    super.initState();
  }

  @override
  void dispose() {
    _spaceSubscription?.cancel();
    super.dispose();
  }
  //Pangea#

  @override
  Widget build(BuildContext context) => InvitationSelectionView(this);
}
