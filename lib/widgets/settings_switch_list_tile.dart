import 'package:flutter/material.dart';

import '../config/app_config.dart';
import 'matrix.dart';

class SettingsSwitchListTile extends StatefulWidget {
  final bool defaultValue;
  final String storeKey;
  final String title;
  //#Pangea
  final String? subtitle;
  //Pangea#
  final Function(bool)? onChanged;

  const SettingsSwitchListTile.adaptive({
    Key? key,
    this.defaultValue = false,
    required this.storeKey,
    required this.title,
    //#Pangea
    this.subtitle,
    //Pangea#
    this.onChanged,
  }) : super(key: key);

  @override
  SettingsSwitchListTileState createState() => SettingsSwitchListTileState();
}

class SettingsSwitchListTileState extends State<SettingsSwitchListTile> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: Matrix.of(context)
          .store
          .getItemBool(widget.storeKey, widget.defaultValue),
      builder: (context, snapshot) => SwitchListTile.adaptive(
        value: snapshot.data ?? widget.defaultValue,
        title: Text(widget.title),
        // #Pangea
        activeColor: AppConfig.activeToggleColor,
        subtitle: widget.subtitle != null ? Text(widget.subtitle!) : null,
        // #Pangea
        onChanged: (bool newValue) async {
          widget.onChanged?.call(newValue);
          await Matrix.of(context).store.setItemBool(widget.storeKey, newValue);
          setState(() {});
        },
      ),
    );
  }
}
