import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:fluffychat/widgets/layouts/empty_page.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:new_version_plus/new_version_plus.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:vrouter/vrouter.dart';

class LoadingView extends StatelessWidget {
  const LoadingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async {
        // #Pangea
        if (!kIsWeb) {
          final newVersionPlus = NewVersionPlus();
          final VersionStatus? status = await newVersionPlus.getVersionStatus();
          if (status != null && status.canUpdate) {
            final confirmed = await showOkCancelAlertDialog(
              context: context,
              title: L10n.of(context)!.appUpdateAvailable,
              message: L10n.of(context)!
                  .updateDesc(status.storeVersion, status.localVersion),
              okLabel: L10n.of(context)!.update,
              cancelLabel: L10n.of(context)!.maybeLater,
            );
            if (confirmed == OkCancelResult.ok) {
              launchUrlString(
                status.appStoreLink,
                mode: LaunchMode.externalApplication,
              );
            }
          }
        }
        // await UpdateCheckerNoStore(context).checkUpdate();
        // #Pangea
        VRouter.of(context).to(
          Matrix.of(context).widget.clients.any(
                    (client) =>
                        client.onLoginStateChanged.value == LoginState.loggedIn,
                  )
              ? '/rooms'
              : '/home',
          queryParameters: VRouter.of(context).queryParameters,
        );
      },
    );
    return const EmptyPage(loading: true);
  }
}
