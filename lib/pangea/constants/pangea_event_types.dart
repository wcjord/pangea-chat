class PangeaEventTypes {
  static const classSettings = "pangea.class";
  static const pangeaExchange = "p.exchange";

  static const rules = "p.rules";

  static const studentAnalyticsSummary = "pangea.usranalytics";

  static const translation = "pangea.translation";
  static const tokens = "pangea.tokens";
  static const choreoRecord = "pangea.record";
  static const representation = "pangea.representation";

  static const vocab = "p.vocab";
  static const roomInfo = "pangea.roomtopic";
}
