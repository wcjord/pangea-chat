import 'package:fluffychat/pages/homeserver_picker/homeserver_picker.dart';
import 'package:fluffychat/pangea/widgets/login/home_picker_logo.dart';
import 'package:fluffychat/widgets/layouts/login_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class PangeaHomeserverPickerView extends StatelessWidget {
  final HomeserverPickerController controller;

  const PangeaHomeserverPickerView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final benchmarkResults = controller.benchmarkResults;
    return LoginScaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            const PangeaLogoAndNameWidget(),
            Container(
              padding: const EdgeInsets.all(12),
              width: double.infinity,
              child: Hero(
                tag: 'loginButton',
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white.withOpacity(0.2),
                      foregroundColor: Colors.white,
                      side: const BorderSide(
                        width: 2,
                        color: Colors.white,
                      ),
                      textStyle: const TextStyle(fontSize: 16),
                    ),
                    onPressed: controller.isLoading
                        ? null
                        : controller.checkHomeserverAction,
                    child: controller.isLoading
                        ? const LinearProgressIndicator()
                        : Text(L10n.of(context)!.connect),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
