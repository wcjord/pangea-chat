import 'package:fluffychat/pangea/widgets/class/add_class_and_invite.dart';
import 'package:fluffychat/pangea/widgets/class/add_space_toggles.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vrouter/vrouter.dart';

class AddExchangeToClass extends StatefulWidget {
  const AddExchangeToClass({Key? key}) : super(key: key);

  @override
  AddExchangeToClassState createState() => AddExchangeToClassState();
}

class AddExchangeToClassState extends State<AddExchangeToClass> {
  final GlobalKey<AddToSpaceState> addToSpaceKey = GlobalKey<AddToSpaceState>();

  @override
  Widget build(BuildContext context) {
    final String spaceId = VRouter.of(context).pathParameters['exchangeid']!;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(L10n.of(context)!.addToClassTitle),
      ),
      body: FutureBuilder(
        future:
            Matrix.of(context).client.waitForRoomInSync(spaceId, join: true),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView(
              children: <Widget>[
                const SizedBox(height: 40),
                AddToSpaceToggles(
                  roomId: VRouter.of(context).pathParameters['exchangeid'],
                  key: addToSpaceKey,
                  startOpen: true,
                  mode: AddToClassMode.exchange,
                ),
              ],
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => VRouter.of(context).to("/rooms"),
        child: const Icon(Icons.arrow_forward_outlined),
      ),
    );
  }
}
