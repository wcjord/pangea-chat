import 'package:fluffychat/pangea/controllers/pangea_controller.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vrouter/vrouter.dart';

class ClassAnalyticsButton extends StatelessWidget {
  const ClassAnalyticsButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final roomId = VRouter.of(context).pathParameters['roomid'];
    final iconColor = Theme.of(context).textTheme.bodyText1!.color;

    return Column(
      children: [
        ListTile(
          title: Text(
            L10n.of(context)!.classAnalytics,
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(L10n.of(context)!.classAnalyticsDesc),
          leading: CircleAvatar(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            foregroundColor: iconColor,
            child: const Icon(Icons.analytics_outlined),
          ),
          onTap: () => VRouter.of(context).to('/analytics/$roomId'),
        ),
      ],
    );
  }
}
