import 'package:fluffychat/pages/archive/archive.dart';
import 'package:fluffychat/pages/chat/chat.dart';
import 'package:fluffychat/pages/chat_details/chat_details.dart';
import 'package:fluffychat/pages/chat_list/chat_list.dart';
import 'package:fluffychat/pages/chat_permissions_settings/chat_permissions_settings.dart';
import 'package:fluffychat/pages/connect/connect_page.dart';
import 'package:fluffychat/pages/device_settings/device_settings.dart';
import 'package:fluffychat/pages/homeserver_picker/homeserver_picker.dart';
import 'package:fluffychat/pages/login/login.dart';
import 'package:fluffychat/pages/new_group/new_group.dart';
import 'package:fluffychat/pages/new_private_chat/new_private_chat.dart';
import 'package:fluffychat/pages/new_space/new_space.dart';
import 'package:fluffychat/pages/settings/settings.dart';
import 'package:fluffychat/pages/settings_3pid/settings_3pid.dart';
import 'package:fluffychat/pages/settings_chat/settings_chat.dart';
import 'package:fluffychat/pages/settings_emotes/settings_emotes.dart';
import 'package:fluffychat/pages/settings_ignore_list/settings_ignore_list.dart';
import 'package:fluffychat/pages/settings_multiple_emotes/settings_multiple_emotes.dart';
import 'package:fluffychat/pages/settings_notifications/settings_notifications.dart';
import 'package:fluffychat/pages/settings_security/settings_security.dart';
import 'package:fluffychat/pages/settings_stories/settings_stories.dart';
import 'package:fluffychat/pages/settings_style/settings_style.dart';
import 'package:fluffychat/pages/sign_up/signup.dart';
import 'package:fluffychat/pangea/guard/p_vguard.dart';
import 'package:fluffychat/pangea/pages/analytics/class_analytics/class_analytics.dart';
import 'package:fluffychat/pangea/pages/analytics/class_list/class_list.dart';
import 'package:fluffychat/pangea/pages/analytics/student_analytics/student_analytics.dart';
import 'package:fluffychat/pangea/pages/class_settings/class_settings_page.dart';
import 'package:fluffychat/pangea/pages/exchange/add_exchange_to_class.dart';
import 'package:fluffychat/pangea/pages/find_partner/find_partner.dart';
import 'package:fluffychat/pangea/pages/p_user_age/p_user_age.dart';
import 'package:fluffychat/pangea/pages/settings_learning/settings_learning.dart';
import 'package:fluffychat/pangea/pages/settings_subscription/settings_subscription.dart';
import 'package:fluffychat/pangea/widgets/class/join_with_link.dart';
import 'package:fluffychat/widgets/layouts/empty_page.dart';
import 'package:fluffychat/widgets/layouts/loading_view.dart';
import 'package:fluffychat/widgets/layouts/side_view_layout.dart';
import 'package:fluffychat/widgets/layouts/two_column_layout.dart';
import 'package:fluffychat/widgets/log_view.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/vrouter.dart';

import '../pages/invitation_selection/invitation_selection.dart';

class AppRoutes {
  final bool columnMode;

  AppRoutes(this.columnMode);

  List<VRouteElement> get routes => [
        // #Pangea
        VGuard(
            beforeUpdate: PAuthGaurd.onPublicUpdate,
            beforeLeave: PAuthGaurd.onPublicLeave,
            beforeEnter: PAuthGaurd.onPublicEnter,
            stackedRoutes: _homeRoutes),
        if (columnMode)
          VGuard(
              beforeEnter: PAuthGaurd.onPrivateUpdate,
              beforeUpdate: PAuthGaurd.onPrivateUpdate,
              stackedRoutes: _tabletRoutes),
        if (!columnMode)
          VGuard(
            //@gabby - what's this for?
            //  beforeEnter: PAuthGaurd.onUpdate,
            beforeUpdate: PAuthGaurd.onPrivateUpdate,
            stackedRoutes: _mobileRoutes,
          ),
        // #Pangea
        ..._homeRoutes,
        if (columnMode) ..._tabletRoutes,
        if (!columnMode) ..._mobileRoutes,
      ];

  List<VRouteElement> get _mobileRoutes => [
        VWidget(
          path: '/rooms',
          widget: const ChatList(),
          stackedRoutes: [
            // #Pangea
            VWidget(
              path: '/mylearning',
              widget: const StudentAnalyticsPage(),
            ),
            VWidget(
              path: '/analytics',
              widget: const AnalyticsClassList(),
              buildTransition: _fadeTransition,
              stackedRoutes: [
                VWidget(
                  path: ':classid',
                  buildTransition: _fadeTransition,
                  widget: const ClassAnalyticsPage(),
                ),
              ],
            ),
            // VWidget(
            //   path: '/stories/create',
            //   widget: const AddStoryPage(),
            // ),
            // VWidget(
            //   path: '/stories/:roomid',
            //   widget: const StoryPage(),
            //   stackedRoutes: [
            //     VWidget(
            //       path: 'share',
            //       widget: const AddStoryPage(),
            //     ),
            //   ],
            // ),
            // #Pangea
            VWidget(
              path: '/spaces/:roomid',
              widget: const ChatDetails(),
              stackedRoutes: _chatDetailsRoutes,
            ),
            VWidget(
              path: ':roomid',
              widget: const ChatPage(),
              stackedRoutes: [
                // #Pangea
                // VWidget(
                //   path: 'encryption',
                //   widget: const ChatEncryptionSettings(),
                // ),
                // Pangea#
                VWidget(
                  path: 'invite',
                  // #Pangea
                  widget: const InvitationSelection(),
                  // widget: const ClassInvitationSelection(),
                  // Pangea#
                ),
                VWidget(
                  path: 'details',
                  widget: const ChatDetails(),
                  stackedRoutes: _chatDetailsRoutes,
                ),
              ],
            ),
            VWidget(
              path: '/settings',
              widget: const Settings(),
              stackedRoutes: _settingsRoutes,
            ),
            VWidget(
              path: '/archive',
              widget: const Archive(),
              stackedRoutes: [
                VWidget(
                  path: ':roomid',
                  widget: const ChatPage(),
                  buildTransition: _dynamicTransition,
                ),
              ],
            ),
            VWidget(
              path: '/newprivatechat',
              widget: const NewPrivateChat(),
            ),
            VWidget(
              // #Pangea
              path: '/newgroup/:spaceid',
              // path: '/newgroup',
              // Pangea#
              widget: const NewGroup(),
            ),
            VWidget(
              path: '/newspace',
              widget: const NewSpace(),
            ),
            // #Pangea
            VWidget(
              path: '/newspace/:newexchange',
              widget: const NewSpace(),
            ),
            VWidget(
              path: '/join_exchange/:exchangeid',
              widget: const AddExchangeToClass(),
            ),
            VWidget(
              path: '/partner',
              widget: const FindPartner(),
            ),
            VWidget(
              path: '/join_with_link',
              widget: const JoinClassWithLink(),
              buildTransition: _dynamicTransition,
            ),
            // Pangea#
          ],
        ),
      ];
  List<VRouteElement> get _tabletRoutes => [
        VNester(
          path: '/rooms',
          widgetBuilder: (child) => TwoColumnLayout(
            mainView: const ChatList(),
            sideView: child,
          ),
          buildTransition: _fadeTransition,
          nestedRoutes: [
            VWidget(
              path: '',
              widget: const EmptyPage(),
              buildTransition: _fadeTransition,
              stackedRoutes: [
                // #Pangea
                // VWidget(
                //   path: '/stories/create',
                //   buildTransition: _fadeTransition,
                //   widget: const AddStoryPage(),
                // ),
                // VWidget(
                //   path: '/stories/:roomid',
                //   buildTransition: _fadeTransition,
                //   widget: const StoryPage(),
                //   stackedRoutes: [
                //     VWidget(
                //       path: 'share',
                //       widget: const AddStoryPage(),
                //     ),
                //   ],
                // ),
                VWidget(
                  path: '/mylearning',
                  widget: const StudentAnalyticsPage(),
                ),
                VWidget(
                  path: '/analytics',
                  widget: const AnalyticsClassList(),
                  buildTransition: _fadeTransition,
                  stackedRoutes: [
                    VWidget(
                      path: ':classid',
                      buildTransition: _fadeTransition,
                      widget: const ClassAnalyticsPage(),
                    ),
                  ],
                ),
                // #Pangea
                VWidget(
                  path: '/spaces/:roomid',
                  widget: const ChatDetails(),
                  buildTransition: _fadeTransition,
                  stackedRoutes: _chatDetailsRoutes,
                ),
                VWidget(
                  path: '/newprivatechat',
                  widget: const NewPrivateChat(),
                  buildTransition: _fadeTransition,
                ),
                VWidget(
                  // #Pangea
                  path: '/newgroup/:spaceid',
                  // path: '/newgroup',
                  // Pangea#
                  widget: const NewGroup(),
                  buildTransition: _fadeTransition,
                ),
                VWidget(
                  path: '/newspace',
                  widget: const NewSpace(),
                  buildTransition: _fadeTransition,
                ),
                //#Pangea
                VWidget(
                  path: '/newspace/:newexchange',
                  widget: const NewSpace(),
                  buildTransition: _fadeTransition,
                ),
                VWidget(
                  path: '/join_exchange/:exchangeid',
                  widget: const AddExchangeToClass(),
                ),
                VWidget(
                  path: '/partner',
                  widget: const FindPartner(),
                ),
                VWidget(
                  path: '/join_with_link',
                  widget: const JoinClassWithLink(),
                  buildTransition: _dynamicTransition,
                ),
                //Pangea#
                VNester(
                  path: ':roomid',
                  widgetBuilder: (child) => SideViewLayout(
                    mainView: const ChatPage(),
                    sideView: child,
                  ),
                  buildTransition: _fadeTransition,
                  nestedRoutes: [
                    VWidget(
                      path: '',
                      widget: const ChatPage(),
                      buildTransition: _fadeTransition,
                    ),
                    // #Pangea
                    // VWidget(
                    //   path: 'encryption',
                    //   widget: const ChatEncryptionSettings(),
                    //   buildTransition: _fadeTransition,
                    // ),
                    // Pangea#
                    VWidget(
                      path: 'details',
                      widget: const ChatDetails(),
                      buildTransition: _fadeTransition,
                      stackedRoutes: _chatDetailsRoutes,
                    ),
                    VWidget(
                      path: 'invite',
                      // #Pangea
                      widget: const InvitationSelection(),
                      // widget: const ClassInvitationSelection(),
                      // Pangea#
                      buildTransition: _fadeTransition,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
        //#Pangea
        VWidget(
          path: '/join_with_link',
          widget: const JoinClassWithLink(),
          buildTransition: _dynamicTransition,
        ),
        //Pangea#
        VWidget(
          path: '/rooms',
          widget: const TwoColumnLayout(
            mainView: ChatList(),
            sideView: EmptyPage(),
          ),
          buildTransition: _fadeTransition,
          stackedRoutes: [
            VNester(
              path: '/settings',
              widgetBuilder: (child) => TwoColumnLayout(
                mainView: const Settings(),
                sideView: child,
              ),
              buildTransition: _dynamicTransition,
              nestedRoutes: [
                VWidget(
                  path: '',
                  widget: const EmptyPage(),
                  buildTransition: _dynamicTransition,
                  stackedRoutes: _settingsRoutes,
                ),
              ],
            ),
            VNester(
              path: '/archive',
              widgetBuilder: (child) => TwoColumnLayout(
                mainView: const Archive(),
                sideView: child,
              ),
              buildTransition: _fadeTransition,
              nestedRoutes: [
                VWidget(
                  path: '',
                  widget: const EmptyPage(),
                  buildTransition: _dynamicTransition,
                ),
                VWidget(
                  path: ':roomid',
                  widget: const ChatPage(),
                  buildTransition: _dynamicTransition,
                ),
              ],
            ),
          ],
        ),
      ];

  List<VRouteElement> get _homeRoutes => [
        VWidget(path: '/', widget: const LoadingView()),
        VWidget(
          path: '/home',
          widget: const HomeserverPicker(),
          buildTransition: _fadeTransition,
          stackedRoutes: [
            VWidget(
              path: 'login',
              widget: const Login(),
              buildTransition: _fadeTransition,
            ),
            VWidget(
              path: 'connect',
              widget: const ConnectPage(),
              buildTransition: _fadeTransition,
              stackedRoutes: [
                VWidget(
                  path: 'login',
                  widget: const Login(),
                  buildTransition: _fadeTransition,
                ),
                VWidget(
                  path: 'signup',
                  widget: const SignupPage(),
                  buildTransition: _fadeTransition,
                ),
                // #Pangea
                VWidget(
                  path: 'user_age',
                  widget: const PUserAge(),
                  buildTransition: _fadeTransition,
                ),
                // #Pangea
              ],
            ),
            VWidget(
              path: 'logs',
              widget: const LogViewer(),
              buildTransition: _dynamicTransition,
            ),
          ],
        ),
      ];

  List<VRouteElement> get _chatDetailsRoutes => [
        VWidget(
          path: 'permissions',
          widget: const ChatPermissionsSettings(),
          buildTransition: _dynamicTransition,
        ),
        // #Pangea
        VWidget(
          path: 'class_settings',
          widget: const ClassSettingsPage(),
          buildTransition: _dynamicTransition,
        ),
        // #Pangea
        VWidget(
          path: 'invite',
          // #Pangea
          widget: const InvitationSelection(),
          // widget: const ClassInvitationSelection(),
          // #Pangea
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'multiple_emotes',
          widget: const MultipleEmotesSettings(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'emotes',
          widget: const EmotesSettings(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'emotes/:state_key',
          widget: const EmotesSettings(),
          buildTransition: _dynamicTransition,
        ),
      ];

  List<VRouteElement> get _settingsRoutes => [
        // #Pangea
        VWidget(
          path: 'learning',
          widget: const SettingsLearning(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'subscription',
          widget: const SubscriptionManagement(),
          buildTransition: _dynamicTransition,
        ),
        // #Pangea
        VWidget(
          path: 'notifications',
          widget: const SettingsNotifications(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'style',
          widget: const SettingsStyle(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'devices',
          widget: const DevicesSettings(),
          buildTransition: _dynamicTransition,
        ),
        VWidget(
          path: 'chat',
          widget: const SettingsChat(),
          buildTransition: _dynamicTransition,
          stackedRoutes: [
            VWidget(
              path: 'emotes',
              widget: const EmotesSettings(),
              buildTransition: _dynamicTransition,
            ),
          ],
        ),
        VWidget(
          path: 'addaccount',
          widget: const HomeserverPicker(),
          buildTransition: _fadeTransition,
          stackedRoutes: [
            VWidget(
              path: 'login',
              widget: const Login(),
              buildTransition: _fadeTransition,
            ),
            VWidget(
              path: 'connect',
              widget: const ConnectPage(),
              buildTransition: _fadeTransition,
              stackedRoutes: [
                VWidget(
                  path: 'login',
                  widget: const Login(),
                  buildTransition: _fadeTransition,
                ),
                VWidget(
                  path: 'signup',
                  widget: const SignupPage(),
                  buildTransition: _fadeTransition,
                ),
              ],
            ),
          ],
        ),
        VWidget(
          path: 'security',
          widget: const SettingsSecurity(),
          buildTransition: _dynamicTransition,
          stackedRoutes: [
            VWidget(
              path: 'stories',
              widget: const SettingsStories(),
              buildTransition: _dynamicTransition,
            ),
            VWidget(
              path: 'ignorelist',
              widget: const SettingsIgnoreList(),
              buildTransition: _dynamicTransition,
            ),
            VWidget(
              path: '3pid',
              widget: const Settings3Pid(),
              buildTransition: _dynamicTransition,
            ),
          ],
        ),
        VWidget(
          path: 'logs',
          widget: const LogViewer(),
          buildTransition: _dynamicTransition,
        ),
      ];

  FadeTransition Function(dynamic, dynamic, dynamic)? get _dynamicTransition =>
      columnMode ? _fadeTransition : null;

  FadeTransition _fadeTransition(animation1, _, child) =>
      FadeTransition(opacity: animation1, child: child);
}
