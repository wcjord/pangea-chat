import 'dart:ui';

import 'package:fluffychat/pangea/config/environment.dart';
import 'package:matrix/matrix.dart';

abstract class AppConfig {
  // #Pangea
  // static String _applicationName = 'FluffyChat';
  static String _applicationName = 'Pangea Chat';
  // #Pangea
  static String get applicationName => _applicationName;
  static String? _applicationWelcomeMessage;
  static String? get applicationWelcomeMessage => _applicationWelcomeMessage;
  // #Pangea
  // static String _defaultHomeserver = 'matrix.org';
  static String _defaultHomeserver = Environment.synapsURL;
  // #Pangea
  static String get defaultHomeserver => _defaultHomeserver;
  static double bubbleSizeFactor = 1;
  static double fontSizeFactor = 1;
  static const Color chatColor = primaryColor;
  static Color? colorSchemeSeed = primaryColor;
  static const double messageFontSize = 15.75;
  static const bool allowOtherHomeservers = true;
  static const bool enableRegistration = true;
  // #Pangea
  // static const Color primaryColor = Color(0xFF5625BA);
  // static const Color primaryColorLight = Color(0xFFCCBDEA);
  static const Color primaryColor = Color(0xFF8560E0);
  static const Color primaryColorLight = Color(0xFFDBC9FF);
  static const Color secondaryColor = Color(0xFF41a2bc);
  static const Color activeToggleColor = Color(0xFF33D057);
  // static String _privacyUrl =
  //     'https://gitlab.com/famedly/fluffychat/-/blob/main/PRIVACY.md';
  static String _privacyUrl = "https://www.pangeachat.com/privacy";
  //Pangea#
  static String get privacyUrl => _privacyUrl;
  static const String enablePushTutorial =
      'https://www.reddit.com/r/fluffychat/comments/qn6liu/enable_push_notifications_without_google_services/';
  static const String encryptionTutorial =
      'https://gitlab.com/famedly/fluffychat/-/wikis/How-to-use-end-to-end-encryption-in-FluffyChat';
  static const String appId = 'im.fluffychat.FluffyChat';
  // #Pangea
  // static const String appOpenUrlScheme = 'im.fluffychat';
  static const String appOpenUrlScheme = 'matrix.pangea.chat';
  // Pangea#
  static String _webBaseUrl = 'https://fluffychat.im/web';
  static String get webBaseUrl => _webBaseUrl;
  //#Pangea
  static const String sourceCodeUrl = 'https://gitlab.com/famedly/fluffychat';
  // static const String supportUrl =
  //     'https://gitlab.com/famedly/fluffychat/issues';
  static const String supportUrl = 'https://www.pangeachat.com/faqs';
  static const String termsOfServiceUrl =
      'https://www.pangeachat.com/terms-of-service';
  //Pangea#
  static final Uri newIssueUrl = Uri(
    scheme: 'https',
    host: 'gitlab.com',
    path: '/famedly/fluffychat/-/issues/new',
  );
  static const bool enableSentry = true;
  static const String sentryDns =
      'https://8591d0d863b646feb4f3dda7e5dcab38@o256755.ingest.sentry.io/5243143';
  //#Pangea
  static bool renderHtml = false;
  // static bool renderHtml = true;
  //Pangea#
  static bool hideRedactedEvents = false;
  static bool hideUnknownEvents = true;
  static bool showDirectChatsInSpaces = true;
  static bool separateChatTypes = false;
  static bool autoplayImages = true;
  //#Pangea
  static bool sendOnEnter = true;
  // static bool sendOnEnter = false;
  //Pangea#
  static bool experimentalVoip = false;
  static const bool hideTypingUsernames = false;
  static const bool hideAllStateEvents = false;
  static const String inviteLinkPrefix = 'https://matrix.to/#/';
  static const String deepLinkPrefix = 'im.fluffychat://chat/';
  static const String schemePrefix = 'matrix:';
  // #Pangea
  // static const String pushNotificationsChannelId = 'fluffychat_push';
  // static const String pushNotificationsChannelName = 'FluffyChat push channel';
  // static const String pushNotificationsChannelDescription =
  //     'Push notifications for FluffyChat';
  // static const String pushNotificationsAppId = 'chat.fluffy.fluffychat';
  // static const String pushNotificationsGatewayUrl =
  //     'https://push.fluffychat.im/_matrix/push/v1/notify';
  // static const String pushNotificationsPusherFormat = 'event_id_only';
  static const String pushNotificationsChannelId = 'pangeachat_push';
  static const String pushNotificationsChannelName = 'Pangea Chat push channel';
  static const String pushNotificationsChannelDescription =
      'Push notifications for Pangea Chat';
  static const String pushNotificationsAppId = 'com.talktolearn.chat';
  static const String pushNotificationsGatewayUrl =
      'https://sygnal.pangea.chat/_matrix/push/v1/notify';
  static const String? pushNotificationsPusherFormat = null;
  // #Pangea
  static const String emojiFontName = 'Noto Emoji';
  static const String emojiFontUrl =
      'https://github.com/googlefonts/noto-emoji/';
  static const double borderRadius = 16.0;
  static const double columnWidth = 360.0;
  static bool hideUnimportantStateEvents = true;

  // #Pangea
  static String googlePlayMangementUrl =
      "https://play.google.com/store/account/subscriptions";
  static String googlePlayHistoryUrl =
      "https://play.google.com/store/account/orderhistory";
  static String googlePlayPaymentMethodUrl =
      "https://play.google.com/store/paymentmethods";
  static String appleMangementUrl =
      "https://apps.apple.com/account/subscriptions";
  static String stripePerMonth =
      "https://buy.stripe.com/test_bIY6ssd8z5Uz8ec8ww";
  static String iosPromoCode =
      "https://apps.apple.com/redeem?ctx=offercodes&id=1445118630&code=";
  // Pangea#

  static void loadFromJson(Map<String, dynamic> json) {
    if (json['chat_color'] != null) {
      try {
        colorSchemeSeed = Color(json['chat_color']);
      } catch (e) {
        Logs().w(
            'Invalid color in config.json! Please make sure to define the color in this format: "0xffdd0000"',
            e);
      }
    }
    if (json['application_name'] is String) {
      _applicationName = json['application_name'];
    }
    if (json['application_welcome_message'] is String) {
      _applicationWelcomeMessage = json['application_welcome_message'];
    }
    if (json['default_homeserver'] is String) {
      _defaultHomeserver = json['default_homeserver'];
    }
    if (json['privacy_url'] is String) {
      _webBaseUrl = json['privacy_url'];
    }
    if (json['web_base_url'] is String) {
      _privacyUrl = json['web_base_url'];
    }
    if (json['render_html'] is bool) {
      // #Pangea
      // this is interfering with our PangeaRichText functionality, removing it for now
      renderHtml = false;
      // renderHtml = json['render_html'];
      // #Pangea
    }
    if (json['hide_redacted_events'] is bool) {
      hideRedactedEvents = json['hide_redacted_events'];
    }
    if (json['hide_unknown_events'] is bool) {
      hideUnknownEvents = json['hide_unknown_events'];
    }
  }
}
