variables:
  FLUTTER_VERSION: 3.10.0

# Build and deploy
stages:
  - update-version
  - build_staging
  - deploy_staging
  - upload_sentry
  - build_prod
  - deploy_prod

increment-version:
  stage: update-version
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /^Merge branch.*into 'pangea-main'/ # trigger the pipeline when merging into pangea-main (when a merge request is approved)
  before_script: # setup credentials for pushing from gitlab CI environment
    - git config --global user.email '${GITLAB_USER_EMAIL}'
    - git config --global user.name '${GITLAB_USER_ID}'
    - git remote rm origin && git remote add origin https://${CI_USERNAME}:${CI_PUSH_TOKEN}@gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}.git
  script:
    - echo "Incrementing version in pubspec.yaml"
    - perl -i -pe 's/^(version:\s+\d+\.\d+\.\d+\+)(\d+)$/$1.($2+1)/e' pubspec.yaml # update the version number
    - git add pubspec.yaml # add to repo and push changes
    - git commit -m "Updated version number [ci skip]"
    - git push origin HEAD:$CI_COMMIT_REF_NAME

build_staging:
  environment: staging
  stage: build_staging
  image: ghcr.io/cirruslabs/flutter:${FLUTTER_VERSION}
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

  before_script:
    - echo "$WEB_APP_ENV" > .env
    - sudo apt update && sudo apt install curl -y
    - chmod -R 0777 scripts
  script:
    - ./scripts/prepare-web.sh
    - ./scripts/build-web.sh

  artifacts:
    paths:
      - ./build/web

upload_sentry:
  stage: upload_sentry
  image: getsentry/sentry-cli
  needs:
    - job: build_staging
      artifacts: true
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

  before_script:
    - chmod -R 0777 scripts
  script:
    - echo "Create a new release $CI_COMMIT_SHA"
    - ./scripts/upload-sentry.sh

deploy_staging:
  environment: staging
  stage: deploy_staging
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

  script:
    - aws s3 sync ./build/web s3://$WEBAPP_S3_BUCKET
    - aws cloudfront create-invalidation --distribution-id $CF_DISTRIBUTION_ID --paths "/*"

build_prod:
  environment: production
  stage: build_prod
  image: ghcr.io/cirruslabs/flutter:${FLUTTER_VERSION}
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  when: manual

  before_script:
    - echo "$WEB_APP_ENV" > .env
    - sudo apt update && sudo apt install curl -y
    - chmod -R 0777 scripts
  script:
    - ./scripts/prepare-web.sh
    - ./scripts/build-web.sh

  artifacts:
    paths:
      - ./build/web

deploy_prod:
  environment: production
  stage: deploy_prod
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  when: manual

  script:
    - aws s3 sync ./build/web s3://$WEBAPP_S3_BUCKET
    - aws cloudfront create-invalidation --distribution-id $CF_DISTRIBUTION_ID --paths "/*"
